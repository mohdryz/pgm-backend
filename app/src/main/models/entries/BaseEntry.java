package models.entries;

import lombok.Data;

import java.io.Serializable;

/**
 * Created By Riyaz
 * On 16 July, 2019.
 */
@Data
public abstract class BaseEntry implements Serializable {
    private Long id;

    private String createdBy;

    private String updatedBy;

    protected Long version;
}
