package models.dao;

import models.entities.BaseEntity;
import lombok.Data;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created By Riyaz
 */

@Data
@Repository
public abstract class BaseDao<T extends BaseEntity> {
    @PersistenceContext
    private EntityManager em;

    //TODO implement generic search method
}
