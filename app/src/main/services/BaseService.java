package services;


import lombok.Getter;
import models.entities.BaseEntity;
import models.entries.BaseEntry;

/**
 * Created By Riyaz
 * On 15 July, 2019
 */
@Getter
public abstract class BaseService<Entity extends BaseEntity, Entry extends BaseEntry> {

    protected abstract Entry convertToEntry(Entity entity, Entry entry);

    protected abstract Entity convertToEntity(Entry entry, Entity entity);

}
