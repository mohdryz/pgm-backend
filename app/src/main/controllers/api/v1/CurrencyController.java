package controllers.api.v1;

import controllers.BaseController;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("/api/v1/currency")
public class CurrencyController extends BaseController {

    @GetMapping(value="/code/{id}", produces = "application/json")
    public HashMap<String, String> getCurrency(@PathVariable int id) {
        HashMap<String, String> resp = new HashMap<>();
        resp.put(String.valueOf(id), "value1");
        return resp;
    }

}
