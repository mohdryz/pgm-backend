package com.livspace.service_framework.codes;

/**
 * Created By Riyaz
 */

public enum SuccessCodes{
    OK(200, "OK"),
    CREATED(201, "Created Successfully"),
    ACCEPTED(202, "Accepted"),
    NO_CONTENT(204, "No Content");

    Integer code;
    String message;

    SuccessCodes(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
