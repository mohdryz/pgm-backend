package controllers.api.v1;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


//@WebMvcTest(value = controllers.api.v1.CurrencyController.class, secure = false)
//@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CurrencyController.class)
public class CurrencyControllerTest {
//    @Autowired
//    private MockMvc mockMvc;

//    @MockBean
//    private CurrencyService currencyService;

    @Test
    public void testGetCurrency1() throws Exception {
//        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/v1/currency/code/23").accept(MediaType.APPLICATION_JSON);
//        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
//        System.out.println(result.getResponse());
//        String expected = "{key:value1}";

//        JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
        Assert.assertEquals(2, Integer.parseInt("2"));
    }

}
